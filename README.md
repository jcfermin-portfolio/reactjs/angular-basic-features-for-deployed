# Angular App

This is a simple Angular application demonstrating routing between pages, form handling, data sharing between components, and using Angular Material for UI.

## Features

- Routing between two pages
- Basic form management with validation
- Data sharing between pages using a service
- Improved UI with Angular Material
- Responsive design
- Normalize.css for consistent styling across browsers

## Prerequisites

- Node.js and npm installed on your machine
- Angular CLI installed globally (`npm install -g @angular/cli`)

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/jcfermin-portfolio/reactjs/angular-basic-features-for-deployed.git
   cd my-angular-app
   npm install
   ng serve
   http://localhost:4200